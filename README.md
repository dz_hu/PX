# 2020Stata暑期班-课程主页


&emsp;



> &#x1F353; [2020暑期班听课建议-连享会](https://www.lianxh.cn/news/69706e871c9ad.html) (NEW！`2020.7.26`)

&emsp; 

> &#x1F34F;  常见问题解答在这里
> &#x1F449;  点击上方【[Wiki](https://gitee.com/arlionn/PX/wikis)】分块查看；点击右上角【[Fork](https://gitee.com/arlionn/PX#)】按钮，把本仓库复制到你的码云页面下。

&emsp; 

> &#x1F332; [课程详情](https://gitee.com/arlionn/PX/blob/master/%E8%AF%BE%E7%A8%8B%E8%AF%A6%E6%83%85.md)，请期待下期课程。

&emsp;

### 分块阅读

--- - --

- 2020暑期Stata研讨班：线上直播！
- ☕ A. [课程概要](https://gitee.com/arlionn/PX/wikis/A.%20%E8%AF%BE%E7%A8%8B%E5%AF%BC%E5%BC%95?sort_id=2306512)
- ☕ B. [授课嘉宾](https://gitee.com/arlionn/PX/wikis/B.%20%E5%98%89%E5%AE%BE%E7%AE%80%E4%BB%8B?sort_id=2306139)
- &#x1F449; 1. [Stata 初级班](https://gitee.com/arlionn/PX/wikis/1.%20%E5%88%9D%E7%BA%A7%E7%8F%AD?sort_id=2306106)
- &#x1F449; 2. [Stata 高级班](https://gitee.com/arlionn/PX/wikis/2.%20%E9%AB%98%E7%BA%A7%E7%8F%AD?sort_id=2306137)
- &#x1F449; 3. [Stata 论文班](https://gitee.com/arlionn/PX/wikis/3.%20%E8%AE%BA%E6%96%87%E7%8F%AD?sort_id=2306138)
- &#x1F308; 4. [报名和缴费](https://gitee.com/arlionn/PX/wikis/4.%20%E6%8A%A5%E5%90%8D%E5%92%8C%E7%BC%B4%E8%B4%B9?sort_id=2306133)
- &#x1F353; 5. [助教招聘](https://gitee.com/arlionn/PX/wikis/5.%20%E5%8A%A9%E6%95%99%E6%8B%9B%E8%81%98?sort_id=2306134)

--- - --

&emsp;

&emsp;

### 相关课程


&emsp; 

> #### [连享会·直播 - DSGE 专题](https://gitee.com/arlionn/DSGE)     
> **线上直播 4 天**：2020.9.19-20; 9.26-27  
> **主讲嘉宾**：朱传奇 (中山大学)  
>    &emsp;    
> **课程主页**：<https://gitee.com/arlionn/DSGE> 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-DSGE-海报600.png)

&emsp;

&emsp;


&emsp;


### 关于我们

**Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。

- [直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [连享会-知乎专栏](https://www.zhihu.com/people/arlionn/) 分享了 200 多篇推文。
- 公众号「Stata连享会」分享最新方法和资讯。

